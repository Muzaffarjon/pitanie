<?php

namespace App\Http\Controllers;

use App\BalanceHistory;
use App\Datefood;
use App\FoodSelect;
use App\Help;
use App\Users;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    /**
     * @return array json
     */
    public function users()
    {
        // Выборка всех активных пользователей без админа
        $users = Users::select([
            'id',
            'name',
            'fname',
            'lname',
            'fio_parents',
            'phone_parents'
        ])
            ->where([
                'isAdmin' => 0,
                'isActive' => 1
            ])
            ->get();

        foreach ($users as $user) {

            $response[] = [
                'id' => $user->id,
                'name' => $user->name,
                'fname' => $user->fname,
                'lname' => $user->lname,
                'fio_parents' => $user->fio_parents,
                'phone_parents' => $user->phone_parents,
                'balance' => round($this->balance($user->id), 2)
            ];
        }
        return response($response, 202);
    }

    /**
     * @return mixed
     */
    public function balance_list()
    {
        $balance = BalanceHistory::orderBy('date', 'DESC')
            ->get();

        foreach ($balance as $bal) {
            $user = Users::find($bal->user_id);
            $response[] = [
                'id' => $bal->id,
                'fio' => $user->fname . ' ' . $user->name . ' ' . $user->lname,
                'money' => $bal->money,
                'date' => $bal->date
            ];
        }

        return response($response, 200);
    }
    //Одиночная выборка пользователя

    /**
     * @param $id
     * @return mixed
     */
    public function getUser(int $id)
    {
        $user = Users::find($id);
        if ($user == null)
            $response = ['error' => true, 'message' => 'User does not exist'];
        else
            $response = ['idUser' => $user->id,
                'fname' => $user->fname,
                'name' => $user->name,
                'lname' => $user->lname,
                'fio_parents' => $user->fio_parents,
                'phone_parents' => $user->phone_parents,
                'bes_zavtrak' => $user->bes_zavtrak,
                'bes_obed' => $user->bes_obed,
            ];
        return response($response, 202);
    }

    /**
     * @param string $password
     * @return mixed
     */
    public function checkPass(string $password)
    {
        $user = Users::where([
            'password' => md5($password)
        ])
            ->first();
        if (!empty($user)) {
            $response = [
                'error' => true,
                'message' => 'Password has exist'
            ];
        } else {
            $response = [
                'error' => false,
                'Password not exist'
            ];
        }
        return response($response, 202);
    }

    /**
     * @param int $user_id
     * @param $date
     * @return mixed
     */
    public function food_select_user(int $user_id, $date)
    {
        $foodselect = FoodSelect::select([
                'id',
                'user_id',
                'date',
                'zavtrak',
                'obed',
                'ujin'
            ])
            ->where([
                'user_id' => $user_id,
                'date' => $date
            ])
            ->first();
        $user = Users::find($user_id);
        if (!empty($foodselect)) {
            $response = [
                'error' => false,
                'data' => $foodselect,
                'bes_zavtrak' => $user->bes_zavtrak,
                'bes_obed' => $user->bes_obed
            ];
        } else {
            $response = [
                'error' => true,
                'message' => 'Date is empty in database'
            ];
        }
        return response($response, 202);
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        $text = Help::select(['text'])->first();
        $response = [
            'error' => false,
            'text' => $text->text
        ];
        return response($response, 202);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function updateText(Request $request)
    {
        $text = Help::find(1);
        $text->text = $request->get('text');
        if ($text->save()) {
            return response([
                'error' => false,
                'message' => 'Text is successfuly updated'
            ], 202);
        } else {
            return response([
                'error' => true,
                'message' => 'There is an error occured!'
            ], 202);
        }
    }


    /**
     * @param $id
     * @param $start_date
     * @param $end_datee
     * @return mixed
     */
    public function getBalance($id, $start_date, $end_datee)
    {
        $date = date('Y-m-d');
        if ($end_datee >= $date) {
            $end_date = $date;
        } else
            $end_date = $end_datee;

        $s_date = '2019-08-01';

        if ($start_date <= $s_date)
            $start_date = $s_date;

        if (isset($id) && isset($start_date)) {

            $balanceList = $this->getBalanceList($id, $start_date, $end_date);

            $eted_for_start_day = $this->checkEatedMoneyWithDate($id, $start_date);

            $eted_for_end_day = $this->checkEatedMoneyWithDate($id, $end_date);

            $ostatok_vhod = $oplata = $this->getBalanceSummWithDate($id, $start_date);
            $ostatok_ishod = $oplata = $this->getBalanceSummWithDate($id, $end_date);

            //summa balanca
            $balanceCurrent = $this->getBalanceSumm($id);

            $summa_z = 0;
            $summa_o = 0;
            $summa_u = 0;
            $i = 0;

            if ($balanceList['count'] > 0) {
                foreach ($balanceList['check'] as $bal) {
                    $i++;

                    if ($balanceList['count'] > 1) {

                        if ($balanceList['count'] == $i) {
                            $start_dat = $this->addPlusDay($balanceList['check'][$balanceList['count'] - 1]->date);
                            //$start_dat = $balanceList['check'][count($balanceList)-1]->date;
                            $en_date = $end_date;
                        } else {
                            if ($i == 1)
                                $start_dat = $start_date;
                            else
                                $start_dat = $this->addPlusDay($balanceList['check'][$i - 1]->date);
                            //$start_dat = $balanceList['check'][$i-1]->date;
                            $en_date = $balanceList['check'][$i]->date;
                        }

                    } else {
                        $start_dat = $start_date;
                        $en_date = $end_date;
                    }

                    $oplata = $this->getBalanceSummWithDate($id, $bal->date);

                    $eted_money_for_the_date = $this->checkEatedMoneyWithDate($id, date('Y-m-d', strtotime($bal->date . ' - 1 day')));

                    $database = FoodSelect::where(['user_id' => $id])->whereBetween('date', [$start_dat, $en_date])->orderBy('date', 'DESC')->get();
                    $bl[] = [
                        'sort' => date('Y-m-d', strtotime($bal->date . ' - 1 day')),
                        'date' => $this->formatDay($bal->date),
                        'money' => $bal->money,
                        'summa_rashod' => '',
                        'v_ostatok' => round($oplata - $eted_money_for_the_date, 2)
                    ];


                    foreach ($database as $date) {

                        $oplata_date = $this->getBalanceSummWithDate($id, $date->date);

                        $eted_money_for_the_date = $this->checkEatedMoneyWithDate($id, $date->date);

                        $sena = $this->getDayPrice($date->date);

                        if (!empty($sena)) {
                            if ($date->zavtrak == 1) {
                                $summa_z += !empty($sena->zavtrak) ? $sena->zavtrak : 0;
                            }

                            if ($date->obed == 1) {
                                $summa_o += !empty($sena->obed) ? $sena->obed : 0;
                            }

                            if ($date->ujin == 1) {
                                $summa_u += !empty($sena->ujin) ? $sena->ujin : 0;
                            }
                            $sena = ($date->zavtrak == 1 ? $sena->zavtrak : 0) + ($date->obed == 1 ? $sena->obed : 0) + ($date->ujin == 1 ? $sena->ujin : 0);
                            $summaAll = $summa_z + $summa_o + $summa_u;


                            $bl[] = [
                                'sort' => $date->date,
                                'date' => $this->formatDay($date->date),
                                'money' => '',
                                'summa_rashod' => round($sena, 2),
                                'v_ostatok' => round($oplata_date - $eted_money_for_the_date, 2),
                            ];

                        }
                    }
                }
                if (!empty($bl))
                    sort($bl);

            } else {
                $database = FoodSelect::where(['user_id' => $id])->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'ASC')->get();
                $summa_z = 0;
                $summa_o = 0;
                $summa_u = 0;
                if (!empty($database)) {
                    foreach ($database as $date) {
                        $sena = $this->getDayPrice($date->date);
                        if (!empty($sena)) {
                            if ($date->zavtrak == 1) {
                                $summa_z += !empty($sena->zavtrak) ? $sena->zavtrak : 0;
                            }

                            if ($date->obed == 1) {
                                $summa_o += !empty($sena->obed) ? $sena->obed : 0;
                            }

                            if ($date->ujin == 1) {
                                $summa_u += !empty($sena->ujin) ? $sena->ujin : 0;
                            }
                            $sena = ($date->zavtrak == 1 ? $sena->zavtrak : 0) + ($date->obed == 1 ? $sena->obed : 0) + ($date->ujin == 1 ? $sena->ujin : 0);
                            $summaAll = $summa_z + $summa_o + $summa_u;


                            $bl[] = [
                                'date' => $this->formatDay($date->date),
                                'money' => '',
                                'summa_rashod' => round($sena, 2),
                                'v_ostatok' => round($balanceCurrent - $summaAll, 2),
                            ];

                        }
                    }
                }
            }
            $response = [
                'reports' => !empty($bl) ? $bl : null,
                'date_start' => $this->formatDay($start_date),
                'ishodyawiy_ostatok' => round($ostatok_vhod - $eted_for_start_day, 2),
                'end_date' => $this->formatDay($end_date),
                'v_ostatok' => round($ostatok_ishod - $eted_for_end_day, 2)
            ];

            return response($response, 200);
        } else {

            return response([
                'error' => true
            ], 200);

        }

    }

    /**
     * @param $date
     * @return string
     */
    private function addPlusDay($date)
    {
        $d = explode('-', $date);
        $y = $d[0];
        $m = $d[1];
        $day = $d[2];
        $day = $day + 1;
        return $y . '-' . $m . '-' . $day;

    }

    /**
     * @param $date
     * @return string
     */
    private function formatDay($date)
    {
        $d = explode('-', $date);
        $y = $d[0];
        $m = $d[1];
        $day = $d[2];
        return $day . '/' . $m . '/' . $y;

    }

    /**
     * @param $id
     * @param $start_date
     * @param $end_date
     * @return int
     */
    private function getSummaWithDate($id, $start_date, $end_date)
    {
        $database = FoodSelect::where([
            'user_id' => $id
        ])
            ->whereBetween('date', [$start_date, $end_date])
            ->get();

        $summa = 0;
        $count_obed = 0;
        $count_zavtrak = 0;
        $count_ujin = 0;
        foreach ($database as $date) {
            $sena = $this->getDayPrice($date->date);
            if ($date->zavtrak == 1) {
                $summa += !empty($sena->zavtrak) ? $sena->zavtrak : 0;
                $count_zavtrak++;
            }

            if ($date->obed == 1) {
                $summa += !empty($sena->obed) ? $sena->obed : 0;
                $count_obed++;
            }

            if ($date->ujin == 1) {
                $count_ujin++;
                $summa += !empty($sena->ujin) ? $sena->ujin : 0;
            }

        }
        return $summa;
    }

    /**
     * @param $id
     * @return int
     */
    public function balance($id)
    {
        return $this->getBalanceSumm($id) - $this->checkEatedMoney($id);
    }

    /**
     * @param $id
     * @return int
     */
    public function checkEatedMoney($id)
    {
        $database = FoodSelect::where([
            'user_id' => $id
        ])
            ->where('date', '<=', date('Y-m-d'))
            ->get();
        $summa = 0;
        foreach ($database as $date) {
            $sena = $this->getDayPrice($date->date);
            if (!empty($sena)) {
                if ($date->zavtrak == 1)
                    $summa += !empty($sena->zavtrak) ? $sena->zavtrak : 0;


                if ($date->obed == 1)
                    $summa += !empty($sena->obed) ? $sena->obed : 0;


                if ($date->ujin == 1)
                    $summa += !empty($sena->ujin) ? $sena->ujin : 0;

            }
        }
        return $summa;
    }

    /**
     * @param $id
     * @param $date_select
     * @return int
     */
    public function checkEatedMoneyWithDate($id, $date_select)
    {

        $database = FoodSelect::where([
            'user_id' => $id
        ])
            ->where('date', '<=', $date_select)
            ->get();

        $summa = 0;

        foreach ($database as $date) {

            $price = $this->getDayPrice($date->date);

            if (!empty($price)) {

                if ($date->zavtrak == 1) {
                    $summa += !empty($price->zavtrak) ? $price->zavtrak : 0;
                }

                if ($date->obed == 1) {
                    $summa += !empty($price->obed) ? $price->obed : 0;
                }

                if ($date->ujin == 1) {
                    $summa += !empty($price->ujin) ? $price->ujin : 0;
                }
            }
        }
        return $summa;
    }


    /**
     * @param $id
     * @return int
     */
    private function getBalanceSumm($id)
    {
        $summ = BalanceHistory::where(['user_id' => $id])->sum('money');

        return $summ ?? 0;
    }

    /**
     * @param $id
     * @param $s_date
     * @return mixed
     */
    private function getBalanceSummWithDate($id, $s_date)
    {
        $summ = BalanceHistory::where([
            'user_id' => $id
        ])
            ->where('date', '<=', $s_date)
            ->sum('money');

        return !is_null($summ) ? $summ : 0;
    }

    /**
     * @param $id
     * @param $date_s
     * @param $date_e
     * @return array|bool
     */
    private function getBalanceList($id, $date_s, $date_e)
    {
        $check = BalanceHistory::select([
            'date',
            'money'
        ])
            ->where([
                'user_id' => $id
            ])
            ->whereBetween('date', [
                $date_s,
                $date_e
            ])
            ->orderBy('date', 'ASC')
            ->get();

        if ($check) {
            return [
                'check' => $check,
                'count' => count($check)
            ];
        }
        return false;
    }


    // Цена на день

    /**
     * @param $date
     * @return array
     */
    private function getDayPrice($date)
    {
        $price = Datefood::select([
            'zavtrak',
            'obed',
            'ujin'
        ])
            ->where([
                'date' => $date
            ])
            ->first();

        return !empty($price) ? $price : [];
    }


}
